const tableName = 'roles';

module.exports.up = async (db) => {
  await db.schema.createTable(tableName, (table) => {
    table.increments('id').primary();
    table.string('name');
    table.string('description');
    table.timestamps();
  });
};

module.exports.down = async (db) => {
  await db.schema.dropTable(tableName);
};

module.exports.configuration = { transaction: true };
