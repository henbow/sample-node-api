const tableName = 'vehicles';

module.exports.up = async (db) => {
  await db.schema.createTable(tableName, (table) => {
    table.increments('id').primary();
    table.integer('seating_capicity');
    table.integer('luggage_capacity');
    table.string('registration_number');
    table.string('photo');
    table.boolean('status');
    table.timestamps();
  });
};

module.exports.down = async (db) => {
  await db.schema.dropTable(tableName);
};

module.exports.configuration = { transaction: true };
