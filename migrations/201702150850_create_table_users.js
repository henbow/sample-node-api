const tableName = 'users';

module.exports.up = async (db) => {
  await db.schema.createTable(tableName, (table) => {
    table.increments('id').primary();
    table.string('username');
    table.string('password');
    table.string('salutation');
    table.string('fullname');
    table.string('email');
    table.string('photo');
    table.boolean('status');
    table.timestamps();
  });
};

module.exports.down = async (db) => {
  await db.schema.dropTable(tableName);
};

module.exports.configuration = { transaction: true };
