import chalk from 'chalk';
import { get, getConfigFiles } from './config';
import c from './config/constants';
import app from './app';
import { connectAsync as mongo } from './lib/persistence/mongo/connect';

const configFiles = getConfigFiles();

const run = async () => {
  // eslint-disable-next-line
  console.log(chalk.bgMagenta(`Loading ${configFiles.length} config file(s):`));
  configFiles.forEach((f) => {
    // eslint-disable-next-line
    console.log(chalk.bgMagenta(`> ${f}`));
  });

  // eslint-disable-next-line no-console
  console.log(chalk.green(`Running on ${chalk.underline(get(c.CONFIG_ENV).toUpperCase())} environment`));

  const port = get(c.CONFIG_PORT);

  // eslint-disable-next-line no-console
  console.log(chalk.yellow('Creating server instance...'));

  try {
    await mongo(
      get(c.CONFIG_MONGODB_URL),
      get(c.CONFIG_MONGODB_OPTIONS),
      get(c.CONFIG_DEBUG));
  } catch (error) {
    // eslint-disable-next-line
    console.log(chalk.bgRed(error.message));
  }

  app.listen(port, () => {
    // eslint-disable-next-line no-console
    console.log(chalk.bgBlue(`${get(c.CONFIG_APP_NAME)} server started on port ${port}`));

    // eslint-disable-next-line no-console
    console.log(chalk.bgBlue(`PID is ${process.pid}`));
  });
};

run();
