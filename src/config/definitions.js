import Promise from 'bluebird';
import c from './constants';

const definitions = {};

definitions[c.CONFIG_APP_NAME] = {
  doc: 'Application name, will be used for logging',
  default: 'sample-api',
  format: String,
};

definitions[c.CONFIG_ENV] = {
  doc: 'Current env, whether it is production, development, staging or test (default development).',
  default: 'development',
  format: ['production', 'development', 'staging', 'test'],
  env: 'NODE_ENV',
};

definitions[c.CONFIG_DEBUG] = {
  doc: 'Turn debugging on/off (default true).',
  default: true,
  format: Boolean,
  env: 'NODE_DEBUG',
};

definitions[c.CONFIG_IP] = {
  doc: 'IP address to bind to (default 127.0.0.1).',
  default: '127.0.0.1',
  format: String,
};

definitions[c.CONFIG_HTTPS] = {
  doc: 'Turn secure http (https) on/off (default false).',
  default: false,
  format: Boolean,
};

definitions[c.CONFIG_PORT] = {
  doc: 'Port to be bound (default 4000).',
  default: 4000,
  format: Number,
};

definitions[c.CONFIG_SECRET] = {
  doc: 'Random chars, numbers, and symbols to be used for encryption.',
  default: 'SecretKeyPleaseChange',
  format: String,
};

definitions[c.CONFIG_API_VERSION] = {
  doc: 'Default API version to use (default v1).',
  default: 'v1',
  format: String,
};

definitions[c.CONFIG_LOGGER_FORMAT] = {
  doc: 'Log format (default tiny).',
  default: 'tiny',
  format: String,
};

definitions[c.CONFIG_TOKEN_HEADER] = {
  doc: 'Token header name that contain auth token.',
  default: c.TOKEN_HEADER,
  format: String,
};

definitions[c.CONFIG_MYSQL_URL] = {
  doc: 'MySQL connection url with the following format: `driver://user:pass@hostname:port/database`',
  default: 'mysql2://root@localhost:3306/sample_dev',
  format: String,
};

definitions[c.CONFIG_MYSQL_OPTIONS] = {
  doc: 'MySQL connection options',
  default: {},
  format: Object,
};

definitions[c.CONFIG_MONGODB_URL] = {
  doc: 'MongoDB connection URL',
  default: 'mongodb://sample-db/sample_dev',
  format: String,
};

definitions[c.CONFIG_MONGODB_OPTIONS] = {
  doc: 'MongoDB connection options, please refer to `http://mongoosejs.com/docs/connections.html`',
  default: { promiseLibrary: Promise },
  format: Object,
};

definitions[c.CONFIG_NEO4J_URL] = {
  doc: 'Neo4j connection URL',
  default: 'bolt://localhost:7687',
  format: String,
};

definitions[c.CONFIG_NEO4J_OPTIONS] = {
  doc: 'Neo4j connection options, please refer to `http://neo4j.com/docs/developer-manual/current/drivers/`',
  default: {},
  format: Object,
};

definitions[c.CONFIG_REDIS_URL] = {
  doc: 'Redis connection URL, please refer to `https://github.com/NodeRedis/node_redis`',
  default: 'redis://127.0.0.1:6379/sample_dev',
  format: String,
};

definitions[c.CONFIG_SENTRY_URL] = {
  doc: 'Sentry URL for exception monitoring',
  default: '',
  format: String,
  env: 'SENTRY_URL',
};

definitions[c.CONFIG_MAIL_HOST] = {
  doc: 'Mail server host',
  default: '',
  format: String,
};

definitions[c.CONFIG_MAIL_PORT] = {
  doc: 'Mail server port',
  default: 2525,
  format: Number,
};

definitions[c.CONFIG_MAIL_MAILJET_API_KEY] = {
  doc: 'MailJet API key',
  default: '',
  format: String,
};

definitions[c.CONFIG_MAIL_MAILJET_API_SECRET] = {
  doc: 'MailJet API secret',
  default: '',
  format: String,
};

definitions[c.CONFIG_MAIL_FROM_ADDRESS] = {
  doc: 'MailJet "From" address',
  default: '',
  format: String,
};

export default definitions;
