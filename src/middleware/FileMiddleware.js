import multer from 'multer';
import config from '../config';
import * as Constants from '../config/constants';

export const fileHandler = async (req, res, next) => {
  console.log(config.get(Constants.FILE_PATH));
  const upload = multer(config.get(Constants.FILE_PATH));
  upload.array('files', 10);

  next();
};

export default {
  fileHandler,
};
