import constant from '../config/constants';
import { get as config } from '../config';
import Session from '../modules/session/model';
import { AuthenticationError } from '../lib/errors';

/**
 * Protect API endpoint using auth token
 * @param req
 * @param res
 * @param next
 */
export const protect = async (req, res, next) => {
  const token = req.header(config(constant.CONFIG_TOKEN_HEADER));

  if (!token) {
    next(new AuthenticationError(`Header ${config(constant.CONFIG_TOKEN_HEADER)} should be present`));
  }

  let person;
  try {
    person = await Session.authenticate(token);
  } catch (err) {
    next(new AuthenticationError('Invalid token', err));
  }

  if (!person) {
    next(new AuthenticationError('Unknow error'));
  }

  req.user = req.person = person;
  next();
};

export default {
  protect,
};
