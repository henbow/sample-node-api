import validate from 'validate.js';
import { UnprocessableEntityError } from '../lib/errors';
/**
 * Shortcut to success response
 * @param req
 * @param res
 * @param next
 */
export const success = (req, res, next) => {
  res.success = (data) => {
    res.status(200);
    res.locals.responseBody = data;
    res.send(data);
  };

  next();
};

/**
 * Simple ping status
 * @param req
 * @param res
 * @param next
 */
export const ping = (req, res, next) => {
  const url = req.path;
  if (url === '/ping') {
    res.status(200);
    res.send('OK');
  } else {
    next();
  }
};

/**
 * Create route validation
 * @param req
 * @param res
 * @param next
 */
export const check = rules => (req, res, next) => {
  const errors = validate(req.body, rules);

  if (errors) {
    next(new UnprocessableEntityError(errors));
  }

  next();
};
