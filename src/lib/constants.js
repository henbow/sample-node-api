export const EMPLOYMENT_TYPE = {
  DRIVER: 'Driver',
  VALET: 'Valet',
  EVENT_PERSONEL: 'Event Personel',
};
export const EMPLOYMENT_STATUS = {
  FULL_TIME: 'Full Time',
  PART_TIME: 'Part Time',
  CDLT: 'CDLT',
};
export const NATIONALITY = {
  SINGAPOREAN: 'Singaporean',
  PERMANENT_RESIDENT: 'Permanent Resident',
};
export const RACE = {
  CHINESE: 'Chinese',
  INDIAN: 'Indian',
  MALAY: 'Malay',
  CAUCASIAN: 'Caucasian',
  OTHERS: 'Others',
};
export const BANK_NAME = {
  POSB: 'POSB',
  DBS: 'DBS',
  UOB: 'UOB',
  OCBC: 'OCBC',
  HSBC: 'HSBC',
  MAYBANK: 'MAYBANK',
  OTHER: 'Other',
};
export const USER_ROLES = {
  SUPER_ADMINISTRATOR: 'Super Administrator',
  ADMINISTRATOR: 'Administrator',
  CUSTOMER_REPRESENTATIVE: 'Customer Representative',
  OPERATIONS: 'Operations',
  THIRD_PARTY_ADMIN: 'Third Party Administrator',
};

export const DRIVING_LICENSE_CLASS_2 = '2';
export const DRIVING_LICENSE_CLASS_3 = '3';
export const DRIVING_LICENSE_CLASS_3VL = '3VL';
export const DRIVING_LICENSE_CLASS_4 = '4';
export const DRIVING_LICENSE_CLASS_4VL = '4VL';
export const DRIVING_LICENSE_CLASS_5 = '5';
