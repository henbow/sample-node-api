import mongoose from 'mongoose';
import Promise from 'bluebird';
import debug from 'debug';
import util from 'util';
import chalk from 'chalk';

Promise.longStackTraces();

// mpromise (mongoose's default promise library) is deprecated
// so we are replacing it with bluebird
mongoose.Promise = Promise;

/**
 * Create logger for mongodb
 * @type {function}
 */
export const logger = debug('mongo');

/**
 * Log mongodb query when debugging mode is on
 * @param {boolean} enable
 */
function enableDebugging(enable = true) {
  if (enable) {
    mongoose.set('debug', (collectionName, method, query, doc) => {
      logger(`${collectionName}.${method}`, util.inspect(query, false, 20), doc);
    });
  } else {
    mongoose.set('debug', () => {});
  }
}

/**
 * Connect to mongodb instance
 * @param {string} dsn
 * @param {Object} options
 * @param {boolean} log
 * @return {Promise}
 */
export function connectAsync(dsn, options = {}, log = false) {
  return new Promise((resolve, reject) => {
    mongoose.connect(dsn, options)
      .then(() => {
        // eslint-disable-next-line
        console.log(chalk.magenta('Connected to MongoDB server!'));

        enableDebugging(log);
        resolve();
      })
      .catch((err) => {
        reject(err);
      });
  });
}

/**
 * Alias for connectAsync()
 * @param {string} dsn
 * @param {Object} options
 * @param {boolean} log
 * @return {Promise}
 */
export function connect(dsn, options = {}, log = false) {
  return connectAsync(dsn, options, log);
}

export default {
  logger,
  connectAsync,
  connect,
};
