const Department = require('../../../../modules/department/model').default;

/**
 * Create Department documents
 * @param  {Array} Departments
 * @return {Promise}
 */
module.exports = (departments) => {
  const savePromises = departments.map((department) => {
    const promise = Department.findOne({ name: department.name }).exec();

    return promise.then((foundDepartment) => {
      // if found skip persistence
      if (foundDepartment) {
        return Promise.resolve();
      }

      return Department(department).save((err) => {
        if (err) throw err;
      });
    });
  });

  return Promise.all(savePromises);
};
