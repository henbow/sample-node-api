const bcrypt = require('bcrypt');
const User = require('../../../../modules/user/model').default;

/**
 * Create user documents
 * @param  {Array} users
 * @return {Promise}
 */
module.exports = (users) => {
  const savePromises = users.map((user) => {
    const promise = User.findOne({ email: user.email }).exec();

    return promise.then((foundUser) => {
      // if found skip persistence
      if (foundUser) {
        return Promise.resolve();
      }

      user.password = bcrypt.hashSync(user.password, 10);
      console.log(user);

      return User(user).save((err) => {
        if (err) throw err;
      });
    });
  });

  return Promise.all(savePromises);
};
