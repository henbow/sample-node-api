const Task = require('../../../src/model/Task').default;

/**
 * Create tasks documents
 * @param  {Array} tasks
 * @return {Promise}
 */
module.exports = (tasks) => {
  const savePromises = tasks.map(task => (
    Task(task).save((err) => {
      if (err) throw err;
    })
  ));

  return Promise.all(savePromises);
};
