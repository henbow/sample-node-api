import { USER_ROLES } from '../../../../lib/constants';

const users = [
  {
    username: 'superadmin',
    email: 'superadmin@sample.com',
    password: 'supersecret123',
    role: USER_ROLES.SUPER_ADMINISTRATOR,
  },
  {
    username: 'admin',
    email: 'admin@sample.com',
    password: 'supersecret123',
    role: USER_ROLES.ADMINISTRATOR,
  },
  {
    username: 'operation1',
    email: 'operation1@sample.com',
    password: 'supersecret123',
    role: USER_ROLES.OPERATIONS,
  },
];

module.exports = users;
