module.exports = [
  {
    type: 'Wish',
    typeID: '585c8af7c6ea85332fc2ed62', // value objectID of Wish or RefferredChild
    title: 'Task 1',
    note: 'this is a note',
    dueDate: '2017-01-01',
    status: 'NEW',
    archived: false,
    stage: 'INTEREST_INDICATED',
    assignee: {
      _id: '585c8af7c6ea85332fc2ed62',
      __t: 'WishCoordinator',
      fullName: 'Janet Dome',
      lastName: 'Doma',
    },
    entitiesLink: [
      {
        _id: 1,
        firstName: 'Sanji',
        lastName: 'Black Foot',
      },
    ],
    cc: [
      {
        _id: 1,
        firstName: 'Gold',
        lastName: 'D Roger',
      },
    ],
    reminders: [
      {
        number: 2,
        unit: 'DAY', // 'DAY'/'WEEK',
        email: true,
        push: false,
      },
      {
        number: 1,
        unit: 'WEEK', // 'WEEK',
        email: true,
        push: false,
      },
    ],
  },
  {
    type: 'RefferredChild',
    typeID: '58a11f53718d4d27039caa3a', // value objectID of Wish or RefferredChild
    title: 'Task 2',
    note: 'this is a note',
    dueDate: '2017-01-01',
    status: 'DONE',
    archived: true,
    stage: 'INTEREST_INDICATED',
    assignee: {
      _id: '585c8af7c6ea85332fc2ed62',
      __t: 'WishCoordinator',
      fullName: 'Andrew',
      lastName: 'Silalahi',
    },
    entitiesLink: [
      {
        _id: 1,
        firstName: 'Joko',
        lastName: 'Widodo',
      },
    ],
    cc: [
      {
        _id: 1,
        firstName: 'Silver',
        lastName: 'D Roger',
      },
    ],
    reminders: [
      {
        number: 2,
        unit: 'DAY', // 'DAY'/'WEEK',
        email: true,
        push: false,
      },
      {
        number: 1,
        unit: 'WEEK', // 'WEEK',
        email: true,
        push: false,
      },
    ],
  },
];
