import _ from 'lodash';
import { labelToArray, objectToStringProps } from '../utils';
import { isString, isFunction } from '../../../utils';

export class ErrorFactory {
  static createInvalidParameterTypeError(name, type) {
    return new Error(`${name} must be type of ${type}`);
  }
}

export class Raw {
  constructor(expr) {
    this.config = { expr };
  }

  expr(expr) {
    this.config.expr = expr;
    return this;
  }

  get query() {
    return this.config.expr;
  }
}

export class Node {
  constructor(alias, labels = [], properties = {}) {
    this.config = { alias, properties };
    this.config.rels = [];
    this.config.labels = labelToArray(labels);
  }

  reset() {
    this.config.rels = [];
    return this;
  }

  rel(rel) {
    // eslint-disable-next-line no-use-before-define
    if (rel instanceof Relation) {
      this.config.rels.push(rel);
      return this;
    } else if (rel instanceof Node) {
      this.config.rels.push(['--', rel]);
      return this;
    } else if (Array.isArray(rel)) {
      const [relType, node] = rel;

      if (!(node instanceof Node)) {
        throw ErrorFactory.createInvalidParameterTypeError('[?, rel]', 'Node');
      }

      if (!_.includes(['--', '-->', '<--'], relType)) {
        throw ErrorFactory.createInvalidParameterTypeError('[relType, ?]]', '--, -->, or <--');
      }

      this.config.rels.push([relType, node]);
      return this;
    }

    throw ErrorFactory.createInvalidParameterTypeError('rel', 'Relation');
  }

  get query() {
    let query = this.config.alias ? this.config.alias : '';

    if (this.config.labels.length) {
      query += ':';
      query += this.config.labels.join(':');
    }

    if (Object.keys(this.config.properties).length) {
      query += ` {${objectToStringProps(this.config.properties)}}`;
    }

    query = `(${query})`;

    this.config.rels.forEach((n) => {
      // eslint-disable-next-line no-use-before-define
      if (n instanceof Relation || n instanceof Node) {
        query += n.query;
      } else if (Array.isArray(n)) {
        const [relType, rel] = n;
        query += `${relType}${rel.query}`;
      }
    });

    return query;
  }
}

export class Match {
  constructor(optional = false) {
    this.config = { optional };
    this.config.nodes = [];
  }

  filter(cb) {
    this.config.nodes = this.config.nodes.filter(cb);
    return this;
  }

  nodes(nodes) {
    if (Array.isArray(nodes)) {
      nodes.forEach(n => this.add(n));
      return this;
    }

    throw ErrorFactory.createInvalidParameterTypeError('nodes', 'Array<Node>');
  }

  add(node) {
    if (node instanceof Node || node instanceof Raw) {
      this.config.nodes.push(node);
      return this;
    }

    throw ErrorFactory.createInvalidParameterTypeError('node', 'Node');
  }

  get query() {
    if (!this.config.nodes.length) {
      return '';
    }

    const match = this.config.optional ? 'OPTIONAL MATCH' : 'MATCH';
    return `${match} ${this.config.nodes.map(n => n.query).join(', ')}`;
  }
}

export class Relation {
  constructor(alias, labels = [], properties = {}, direction = 'omni', range) {
    this.config = { alias, properties, range };
    this.config.labels = labelToArray(labels);
    this.dir(direction);
  }

  dir(direction) {
    if (!_.includes(['left', 'right', 'omni'], direction)) {
      throw ErrorFactory.createInvalidParameterTypeError('direction', '(left|right|omni)');
    }

    this.config.direction = direction;
    return this;
  }

  direction(direction) {
    return this.dir(direction);
  }

  range(range) {
    this.config.range = range;
    return this;
  }

  node(node) {
    if (node instanceof Node || node instanceof Raw || node === null) {
      this.config.node = node;
      return this;
    }

    throw ErrorFactory.createInvalidParameterTypeError('node', 'Node');
  }

  get query() {
    let query = this.config.alias ? this.config.alias : '';

    if (this.config.labels) {
      query += ':';
      query += this.config.labels.join(':');
    }

    if (Object.keys(this.config.properties).length) {
      query += ` {${objectToStringProps(this.config.properties)}}`;
    }

    const range = isString(this.config.range) ? this.config.range : '';
    query = `[${query}${range}]`;

    switch (this.config.direction) {
      case 'left':
        query = `<-${query}-`;
        break;

      case 'omni':
        query = `-${query}-`;
        break;

      case 'right':
        query = `-${query}->`;
        break;

      default:
        throw new Error('Invalid direction type');
    }

    if (this.config.node) {
      query += this.config.node.query;
    }

    return query;
  }
}

export class Where {
  constructor(expr) {
    this.config = { expr };
  }

  expr(expr) {
    this.config.expr = expr;
    return this;
  }

  get query() {
    return this.config.expr ? `WHERE ${this.config.expr}` : '';
  }
}

export class OrderBy {
  constructor(name, direction) {
    this.config = {};
    this.config.fields = [];

    if (name) {
      this.add(name, direction);
    }
  }

  fields() {
    return this.config.fields;
  }

  add(name, direction) {
    if (name) {
      const order = [name, direction];
      this.config.fields.push(order);
    }

    return this;
  }

  get query() {
    const order = this.config.fields
      .map(([k, v = '']) => `${k} ${v}`)
      .join(', ');
    return order ? `ORDER BY ${order}` : '';
  }
}

export class Create {
  constructor(node) {
    this.config = {};
    this.config.nodes = [];
    this.config.returns = [];

    if (node instanceof Node) {
      this.add(node);
    } else if (Array.isArray(node)) {
      node.forEach(n => this.add(n));
    }
  }

  add(node) {
    if (node instanceof Node) {
      this.config.nodes.push(node);
      return this;
    }

    throw ErrorFactory.createInvalidParameterTypeError('node', 'Node');
  }

  returns(values = '*') {
    if (isString(values)) {
      this.config.returns = values.trim().split(',');
      return this;
    } else if (Array.isArray(values)) {
      this.config.returns = values;
      return this;
    }

    throw ErrorFactory.createInvalidParameterTypeError('node', 'Array<string>|string');
  }

  get query() {
    if (!this.config.nodes.length) {
      return '';
    }

    let query = `CREATE ${this.config.nodes.map(n => n.query).join(', ')}`;

    if (this.returns) {
      query += ` RETURN ${this.config.returns.join(', ')}`;
    }

    return query;
  }
}

export class MatchBuilder {
  constructor(match) {
    this.config = {
      matches: [],
      returns: [],
      orderBy: null,
      where: null,
      limit: null,
      skip: null,
      create: null,
    };

    if (match instanceof Match) {
      this.add(match);
    }
  }

  create() { // eslint-disable-line class-methods-use-this
    throw new Error('Method not implemented');
  }

  add(match) {
    this.config.matches.push(match);
    return this;
  }

  where(condition) {
    if (condition instanceof Where) {
      this.config.where = condition;
      return this;
    }

    if (!this.config.where) {
      this.config.where = new Where();
    }

    if (isString(condition)) {
      this.config.where.expr(condition);
    } else if (isFunction(condition)) {
      condition(this.config.where);
    }

    return this;
  }

  orderBy(name, direction) {
    if (name instanceof OrderBy) {
      this.config.orderBy = name;
      return this;
    }

    if (!this.config.orderBy) {
      this.config.orderBy = new OrderBy();
    }

    if (isFunction(name)) {
      name(this.config.orderBy);
    } else if (isString(name)) {
      this.config.orderBy.add(name, direction);
    }

    return this;
  }

  order(name, direction) {
    return this.orderBy(name, direction);
  }

  skip(skip) {
    this.config.skip = skip;
    return this;
  }

  limit(limit) {
    this.config.limit = limit;
    return this;
  }

  returns(values = '*') {
    if (isString(values)) {
      this.config.returns = values.trim().split(',');
      return this;
    } else if (Array.isArray(values)) {
      this.config.returns = values;
      return this;
    }

    throw ErrorFactory.createInvalidParameterTypeError('node', 'Array<string>|string');
  }

  get query() {
    let query = this.config.matches.map(n => n.query).join(' ');

    if (this.config.where instanceof Where) {
      query += ` ${this.config.where.query}`;
    }

    if (this.config.returns.length) {
      query += ` RETURN ${this.config.returns.join(', ')}`;
    }

    if (this.config.orderBy) {
      query += ` ${this.config.orderBy.query}`;
    }

    if (this.config.skip) {
      query += ` SKIP ${this.config.skip}`;
    }

    if (this.config.limit) {
      query += ` LIMIT ${this.config.limit}`;
    }

    return query;
  }
}
