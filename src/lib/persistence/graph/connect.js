import { v1 as neo4j } from 'neo4j-driver';
import Promise from 'bluebird';
import url from 'url';

/**
 * Create new neo4j connection
 * @todo Fix bug when password contains colon ':'
 * @param {string} dsn
 * @returns {neo4j.driver}
 */
function createConnection(dsn) {
  const parts = url.parse(dsn.indexOf('://') < 0 ? `bolt://${dsn}` : dsn);
  const port = parts.port || 7687;
  const auth = parts.auth || '';
  const [user = '', password = ''] = auth.split(':');
  return neo4j.driver(`bolt://${parts.hostname}:${port}`, neo4j.auth.basic(user, password));
}

/**
 * Create connection asynchronously
 * @param {string} dsn
 * @returns {Promise}
 */
export function connectAsync(dsn) {
  return new Promise((resolve, reject) => {
    try {
      resolve(createConnection(dsn));
    } catch (e) {
      reject(e);
    }
  });
}

/**
 * Public method for create connection
 * @param {string} dsn
 * @returns {neo4j.driver}
 */
export function connect(dsn) {
  return createConnection(dsn);
}
