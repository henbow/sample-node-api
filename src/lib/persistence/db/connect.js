import Promise from 'bluebird';
import knex from 'knex';

/**
 * Create & test database connection
 * @param {string} dsn
 * @returns {Promise}
 */
export async function connectAsyncAndValidate(dsn) {
  return new Promise((resolve, reject) => {
    const conn = knex(dsn);
    conn.raw('SELECT 1+1 AS result')
      .then(() => resolve(conn))
      .catch(err => reject(err));
  });
}

/**
 * Create new knex instance
 * @param {string} dsn
 * @returns {knex}
 */
export function connect(dsn) {
  return knex(dsn);
}
