/**
 * @param {Object} arr
 * @returns {boolean}
 */
export function isArray(arr) {
  return Array.isArray(arr);
}

/**
 * @param {Object} obj
 * @returns {boolean}
 */
export function isObject(obj) {
  return !isArray(obj) && (obj !== null) && (typeof obj === 'object');
}

/**
 * @param {Function} f
 * @returns {boolean}
 */
export function isFunction(f) {
  return typeof f === 'function';
}

/**
 * @param {String} str
 * @returns {boolean}
 */
export function isString(str) {
  return typeof str === 'string';
}

/**
 * @param {Function|Promise} f
 * @returns {boolean}
 */
export function maybeAsync(f) {
  if (!f) {
    return false;
  }

  // eslint-disable-next-line dot-notation
  if (typeof f === 'object' && typeof f['then'] === 'function') {
    return true;
  }

  if (!isFunction(f)) {
    return false;
  }

  if (f.toString().toLowerCase().startsWith('async')) {
    return true;
  }

  return false;
}
