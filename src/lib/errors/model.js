import ExtendableError from 'es6-error';

class ModelError extends ExtendableError {}

/**
 * ModelNotFound
 */
export class ModelNotFoundError extends ModelError {
  constructor(message, model) {
    super(message);
    this.model = model || null;
  }
}

export class SessionNotValid extends ModelError {
  //
}

export class NoResultFoundError extends ModelError {}
