import status from 'http-status';
import ExtendableError from 'es6-error';
import config from '../../config';

export class RuntimeError extends Error {}
export class ServiceNotFoundError extends RuntimeError {}
export class ConfigurationError extends RuntimeError {}

/**
 * APIError
 */
export class APIError extends ExtendableError {
  /**
   * APIError constructor
   *
   * @param message
   * @param httpStatus
   * @param previousError
   */
  constructor(message, httpStatus, previousError) {
    /* eslint-disable no-param-reassign */
    if (message instanceof Error) {
      previousError = message;
      message = previousError.message;
    }

    super(message);
    this.httpStatus = httpStatus || status.INTERNAL_SERVER_ERROR;
    this.previousError = previousError;
  }

  /**
   * Send error to express response
   *
   * @param res
   */
  send(res) {
    res.status(this.httpStatus);
    const body = { message: this.message };
    if (config.get('debug')) {
      body.previousError = this.previousError;
    }
    res.send(body);
  }
}

/**
 * AuthorizationError
 */
export class AuthorizationError extends APIError {
  constructor(message, previousError) {
    super(message, status.UNAUTHORIZED, previousError);
  }
}

/**
 * AuthenticationError
 */
export class AuthenticationError extends APIError {
  constructor(message, previousError) {
    super(message, status.FORBIDDEN, previousError);
  }
}

/**
 * BadRequestError
 */
export class BadRequestError extends APIError {
  constructor(message, previousError) {
    super(message, status.BAD_REQUEST, previousError);
  }
}

/**
 * UnprocessableEntityError
 */
export class UnprocessableEntityError extends APIError {
  /**
   * @param  {Object} errors         validation constraints error messages
   * @param  {Object} previousError
   */
  constructor(errors, previousError) {
    if (!errors) {
      throw new Error('Need constraint errors Object as first argument');
    }
    super('Cannot process your request', 422, previousError);
    this.constraintErrors = errors;
  }
}

/**
 * InternalServerError
 */
export class InternalServerError extends APIError {
  constructor(message, previousError) {
    super(message, status.INTERNAL_SERVER_ERROR, previousError);
  }
}

/**
 * NotFoundError
 */
export class NotFoundError extends APIError {
  constructor(message, previousError) {
    super(message, status.NOT_FOUND, previousError);
  }
}

export default {
  RuntimeError,
  ServiceNotFoundError,
  ConfigurationError,
  APIError,
  AuthorizationError,
  AuthenticationError,
  BadRequestError,
  InternalServerError,
  NotFoundError,
};
