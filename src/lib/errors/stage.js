/* eslint-disable import/prefer-default-export */
import ExtendableError from 'es6-error';

export class StageError extends ExtendableError {}
