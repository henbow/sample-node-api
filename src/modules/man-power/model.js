import mongoose from 'mongoose';
import paginate from 'mongoose-paginate';

import {
  RACE,
  BANK_NAME,
  NATIONALITY,
  EMPLOYMENT_TYPE,
  EMPLOYMENT_STATUS,
  DRIVING_LICENSE_CLASS_2,
  DRIVING_LICENSE_CLASS_3,
  DRIVING_LICENSE_CLASS_3VL,
  DRIVING_LICENSE_CLASS_4,
  DRIVING_LICENSE_CLASS_4VL,
  DRIVING_LICENSE_CLASS_5,
} from '../../lib/constants';

const Schema = mongoose.Schema;
const { ObjectId } = Schema.Types;

/**
 * ManPower Schema
 */
export const ManPowerSchema = new mongoose.Schema({
  _type: { type: String, default: 'Man Power' },
  userId: {
    type: ObjectId,
    ref: 'User',
    index: true,
  },
  idNumber: String,
  fullName: String,
  birthdate: Date,
  nationality: {
    type: String,
    enum: [
      NATIONALITY.SINGAPOREAN,
      NATIONALITY.PERMANENT_RESIDENT,
    ],
    default: NATIONALITY.SINGAPOREAN,
  },
  race: {
    type: String,
    enum: [
      RACE.CAUCASIAN,
      RACE.CHINESE,
      RACE.INDIAN,
      RACE.MALAY,
      RACE.OTHERS,
    ],
    default: RACE.CHINESE,
  },
  phoneNumber: String,

  employmentType: {
    type: String,
    enum: [
      EMPLOYMENT_TYPE.DRIVER,
      EMPLOYMENT_TYPE.VALET,
      EMPLOYMENT_TYPE.EVENT_PERSONEL,
    ],
    default: EMPLOYMENT_TYPE.DRIVER,
  },
  employmentStatus: {
    type: String,
    enum: [
      EMPLOYMENT_STATUS.FULL_TIME,
      EMPLOYMENT_STATUS.PART_TIME,
      EMPLOYMENT_STATUS.CDLT,
    ],
    default: EMPLOYMENT_STATUS.FULL_TIME,
  },
  department: {
    type: ObjectId,
    ref: 'Department',
    index: true,
    required: false,
  },

  bankName: {
    type: String,
    enum: [
      BANK_NAME.DBS,
      BANK_NAME.HSBC,
      BANK_NAME.MAYBANK,
      BANK_NAME.OCBC,
      BANK_NAME.POSB,
      BANK_NAME.UOB,
      BANK_NAME.OTHER,
    ],
    default: BANK_NAME.DBS,
  },
  bankNameOther: String,
  bankAccountName: String,
  bankAccountNumber: String,
  drivingLicenseClasses: [{
    type: String,
    enum: [
      DRIVING_LICENSE_CLASS_2,
      DRIVING_LICENSE_CLASS_3,
      DRIVING_LICENSE_CLASS_3VL,
      DRIVING_LICENSE_CLASS_4,
      DRIVING_LICENSE_CLASS_4VL,
      DRIVING_LICENSE_CLASS_5,
    ],
    default: DRIVING_LICENSE_CLASS_2,
  }],
  drivingLicenseNumber: String,
  drivingHistory: String,
  driverRating: Number,
  driverLicensePhotos: {
    frontSide: { type: String },
    backSide: { type: String },
  },
  ratingComment: String,
  profilePicture: String,
  status: { type: Boolean, default: true },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
}, {
  collection: 'man_powers',
});

/**
 * Instance methods
 */
ManPowerSchema.method({

});

/**
 * Static methods
 */
ManPowerSchema.statics = {

};

ManPowerSchema.plugin(paginate);

export default mongoose.model('ManPower', ManPowerSchema);
