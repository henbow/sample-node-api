import httpStatus from 'http-status';
import paginate from 'express-paginate';

import service from './service';
import { InternalServerError, NotFoundError, ModelNotFoundError } from '../../lib/errors';

export class ManPowerController {
  constructor(options) {
    this.options = options;
  }

  /**
   * Get ManPower list.
   * @return {Function}
   */
  list() {
    return async (req, res, next) => {
      try {
        const { limit = 10, skip = 0, page = 1, ...restQuery } = req.query;

        const results = await this.options.service.list({
          limit: parseInt(limit, 10),
          skip: parseInt(skip, 10),
          page: parseInt(page, 10),
          restQuery,
        });

        return res.status(httpStatus.OK).json({
          query: restQuery,
          limit: parseInt(limit, 10),
          skip: parseInt(skip, 10),
          totalResults: results.total,
          results: results.docs,
          pageCount: results.pages,
          pages: paginate.getArrayPages(req)(limit, results.pages, results.page),
        });
      } catch (err) {
        return next(new InternalServerError(err));
      }
    };
  }

  show() {
    return async (req, res, next) => {
      try {
        const id = req.params.id;
        const manPower = await this.options.service.findById(id);

        return res.status(httpStatus.OK).json(manPower);
      } catch (err) {
        if (err instanceof ModelNotFoundError) {
          return next(new NotFoundError(`ManPower with id ${req.params.id} is not found`, err));
        }
        return next(new InternalServerError(err));
      }
    };
  }

  /**
   * Create new ManPower
   * @type {Function}
   */
  create() {
    return async (req, res, next) => {
      try {
        const manPowerId = req.body.manPowerId;
        const existingManPower = await this.options.service.findByManPowerId(manPowerId);

        if (!existingManPower || existingManPower.length === 0) {
          const newManPower = await this.options.service.create(req.body);
          return res.status(httpStatus.CREATED).json(newManPower);
        }

        return res.status(httpStatus.BAD_REQUEST).json({
          status: httpStatus.BAD_REQUEST,
          message: `ManPower data with Man Power ID ${manPowerId} is already exists.`,
        });
      } catch (err) {
        return next(new InternalServerError(err));
      }
    };
  }

  update() {
    return async (req, res, next) => {
      try {
        const data = req.body;
        data._id = req.params.id; // eslint-disable-line

        const manPower = await this.options.service.update(data);

        return res.status(httpStatus.OK).json(manPower);
      } catch (err) {
        return next(new InternalServerError(err));
      }
    };
  }

  remove() {
    return async (req, res, next) => {
      try {
        const id = req.params.id;
        await this.options.service.remove(id);

        return res.status(httpStatus.OK).json({
          message: 'Success deleting Man Power data',
        });
      } catch (err) {
        return next(new NotFoundError(err));
      }
    };
  }
}

export default new ManPowerController({
  service,
});
