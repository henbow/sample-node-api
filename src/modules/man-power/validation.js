const manPowerRules = {
  idNumber: {
    presence: true,
  },
  fullName: {
    presence: true,
  },
  birthdate: {
    presence: true,
  },
  nationality: {
    presence: true,
  },
  race: {
    presence: true,
  },
  phoneNumber: {
    presence: true,
  },
  employmentType: {
    presence: true,
  },
  employmentStatus: {
    presence: true,
  },
  department: {
    presence: true,
  },
  bankName: {
    presence: true,
  },
  bankAccountName: {
    presence: true,
  },
  bankAccountNumber: {
    presence: true,
  },
  drivingLicenseNumber: {
    presence: true,
  },
  driverLicensePhotos: {
    presence: false,
  },
  profilePicture: {
    presence: false,
  },
};

export default {
  manPowerRules,
};
