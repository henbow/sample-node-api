import moment from 'moment';

import ManPower from './model';
import { NoResultFoundError, NotFoundError, ModelNotFoundError } from '../../lib/errors';

export class ManPowerService {
  constructor(options) {
    this.options = options;
  }

  async list({ limit = 50, page = 1, query = {} } = {}) {
    try {
      const sort = { createdAt: -1 };
      const results = await this.options.ManPower.paginate(query, { limit, page, sort });

      return results;
    } catch (err) {
      throw new NoResultFoundError('No results found!', 'ManPower');
    }
  }

  async findById(id) {
    try {
      return await this.options.ManPower.findById(id);
    } catch (err) {
      throw new ModelNotFoundError(err.message);
    }
  }

  async findByEmail(emailAddress) {
    try {
      return await this.options.ManPower.findOne({ email: emailAddress });
    } catch (err) {
      throw new ModelNotFoundError(err.message);
    }
  }

  async findByManPowerId(mpId) {
    try {
      return await this.options.ManPower.findOne({ manPowerId: mpId });
    } catch (err) {
      throw new ModelNotFoundError(err.message);
    }
  }

  async create(data) {
    try {
      const manPower = this.options.ManPower;
      return await manPower.create(data);
    } catch (err) {
      throw err;
    }
  }

  async update(data) {
    try {
      const id = data._id; // eslint-disable-line
      data.updatedAt = moment().toISOString();

      return await this.options.ManPower.findOneAndUpdate(
        { _id: id },
        { $set: data },
        { new: true },
      );
    } catch (err) {
      throw err;
    }
  }

  async remove(id) {
    try {
      const manPower = await this.options.ManPower.findById(id);
      return await manPower.remove();
    } catch (err) {
      throw new NotFoundError('No data found for deletion!');
    }
  }

  // eslint-disable-next-line
  convertHttpQueryToMongooseCriteria(query) {
    const criteria = { __t: 'ManPower' };

    if (query.keyword) {
      criteria.$or = [
        { fullName: { $regex: new RegExp(`.*${query.keyword}.*`) } },
        { email: { $regex: new RegExp(`.*${query.keyword}.*`) } },
      ];
    }

    if (query.status) {
      criteria.status = query.status || null;
    }

    if (query.dateStart && query.dateEnd) {
      criteria.createdAt = {
        $gte: `${query.dateStart} 00:00:00`,
        $lte: `${query.dateEnd} 23:59:59`,
      };
    }

    delete query.type;

    return criteria;
  }
}

export default new ManPowerService({
  ManPower,
});
