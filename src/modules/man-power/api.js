import express from 'express';

import controller from './controller';
import validation from './validation';
import { check } from '../../middleware/UtilsMiddleware';
import { protect } from '../../middleware/AuthMiddleware';

const api = express.Router();
const manPowerRouter = express.Router();

manPowerRouter.route('/')
  .get(protect, controller.list())
  .post(protect, check(validation.manPowerRules), controller.create());

manPowerRouter.route('/:id')
  .get(protect, controller.show())
  .put(protect, controller.update())
  .delete(protect, controller.remove());

api.use('/manpower', manPowerRouter);

export default api;
