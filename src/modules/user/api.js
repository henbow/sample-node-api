import express from 'express';

import controller from './controller';
import validation from './validation';
import { check } from '../../middleware/UtilsMiddleware';
import { protect } from '../../middleware/AuthMiddleware';

const api = express.Router();
const userRouter = express.Router();

userRouter.route('/')
  .get(protect, controller.list())
  .post(protect, check(validation.user), controller.create());

userRouter.route('/:id')
  .get(protect, controller.show())
  .put(protect, controller.update())
  .delete(protect, controller.remove());

api.use('/user', userRouter);

export default api;
