import c from '../../config/constants';
import api from './api';

function configure(app) {
  app.on(c.MODULE_ON_INIT, () => {
    // eslint-disable-next-line
    console.log('User module is loaded.');
  });

  app.on(c.MODULE_ON_EXPOSE, () => {
    app.use(api);
  });
}

export default { configure };
