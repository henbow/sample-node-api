import bcrypt from 'bcrypt';
import moment from 'moment';

import User from './model';
import { NoResultFoundError, NotFoundError, ModelNotFoundError } from '../../lib/errors';

export class UserService {
  constructor(options) {
    this.options = options;
  }

  async list({ limit = 50, page = 1, query = {} } = {}) {
    try {
      const sort = { createdAt: -1 };
      const results = await this.options.User.paginate(query, { limit, page, sort });

      return results;
    } catch (err) {
      throw new NoResultFoundError('No results found!', 'User');
    }
  }

  async findById(id) {
    try {
      return await this.options.User.findById(id);
    } catch (err) {
      throw new ModelNotFoundError(err.message);
    }
  }

  async findByEmail(emailAddress) {
    try {
      return await this.options.User.findOne({ email: emailAddress });
    } catch (err) {
      throw new ModelNotFoundError(err.message);
    }
  }

  async create(data) {
    try {
      const user = this.options.User;
      data.password = bcrypt.hashSync(data.password, 10);
      return await user.create(data);
    } catch (err) {
      throw err;
    }
  }

  async update(data) {
    try {
      const id = data._id; // eslint-disable-line
      data.updatedAt = moment().toISOString();

      return await this.options.User.findOneAndUpdate(
        { _id: id },
        { $set: data },
        { new: true },
      );
    } catch (err) {
      throw err;
    }
  }

  async remove(id) {
    try {
      const user = await this.options.User.findById(id);
      return await user.remove();
    } catch (err) {
      throw new NotFoundError('No data found for deletion!');
    }
  }

  // eslint-disable-next-line
  convertHttpQueryToMongooseCriteria(query) {
    const criteria = { __t: 'User' };

    if (query.keyword) {
      criteria.$or = [
        { fullName: { $regex: new RegExp(`.*${query.keyword}.*`) } },
        { email: { $regex: new RegExp(`.*${query.keyword}.*`) } },
      ];
    }

    if (query.status) {
      criteria.status = query.status || null;
    }

    if (query.dateStart && query.dateEnd) {
      criteria.createdAt = {
        $gte: `${query.dateStart} 00:00:00`,
        $lte: `${query.dateEnd} 23:59:59`,
      };
    }

    delete query.type;

    return criteria;
  }
}

export default new UserService({
  User,
});
