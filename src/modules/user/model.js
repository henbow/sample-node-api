import mongoose from 'mongoose';
import paginate from 'mongoose-paginate';
import bcrypt from 'bcrypt';

import { InternalServerError } from '../../lib/errors';
import { USER_ROLES } from '../../lib/constants';

const Schema = mongoose.Schema;
const { ObjectId } = Schema.Types;

/**
 * User Schema
 */
export const UserSchema = new mongoose.Schema({
  _type: { type: String, default: 'User' },
  password: String,
  role: {
    type: String,
    enum: [
      USER_ROLES.SUPER_ADMINISTRATOR,
      USER_ROLES.ADMINISTRATOR,
      USER_ROLES.CUSTOMER_REPRESENTATIVE,
      USER_ROLES.OPERATIONS,
      USER_ROLES.THIRD_PARTY_ADMIN,
    ],
    default: USER_ROLES.OPERATIONS,
  },
  username: String,
  email: String,
  department: {
    type: ObjectId,
    ref: 'Department',
    index: true,
    required: false,
  },
  status: { type: Boolean, default: true },
  lastLogin: { type: Date },
  deletedAt: { type: Date },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
}, {
  collection: 'users',
});

/**
 * Instance methods
 */
UserSchema.method({

  /**
   * Check for user password
   *
   * @param password
   * @returns {boolean}
   */
  async checkPassword(password) {
    let matched;
    try {
      matched = bcrypt.compareSync(password, this.password);
    } catch (err) {
      throw new InternalServerError('Unknown errors', err);
    }

    return matched;
  },

});

/**
 * Static methods
 */
UserSchema.statics = {

};

UserSchema.plugin(paginate);

export default mongoose.model('User', UserSchema);
