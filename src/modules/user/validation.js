const user = {
  fullName: {
    presence: true,
  },
  email: {
    presence: true,
  },
  password: {
    presence: true,
  },
  role: {
    presence: true,
  },
};

export default {
  user,
};
