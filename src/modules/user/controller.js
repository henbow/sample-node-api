import httpStatus from 'http-status';
import paginate from 'express-paginate';

import service from './service';
import { InternalServerError, NotFoundError, ModelNotFoundError } from '../../lib/errors';

export class UserController {
  constructor(options) {
    this.options = options;
  }

  /**
   * Get User list.
   * @return {Function}
   */
  list() {
    return async (req, res, next) => {
      try {
        const { limit = 10, skip = 0, page = 1, ...restQuery } = req.query;

        const results = await this.options.service.list({
          limit: parseInt(limit, 10),
          skip: parseInt(skip, 10),
          page: parseInt(page, 10),
          restQuery,
        });

        return res.status(httpStatus.OK).json({
          query: restQuery,
          limit: parseInt(limit, 10),
          skip: parseInt(skip, 10),
          totalResults: results.total,
          results: results.docs,
          pageCount: results.pages,
          pages: paginate.getArrayPages(req)(limit, results.pages, results.page),
        });
      } catch (err) {
        return next(new InternalServerError(err));
      }
    };
  }

  show() {
    return async (req, res, next) => {
      try {
        const id = req.params.id;
        const user = await this.options.service.findById(id);

        return res.status(httpStatus.OK).json(user);
      } catch (err) {
        if (err instanceof ModelNotFoundError) {
          return next(new NotFoundError(`User with id ${req.params.id} is not found`, err));
        }
        return next(new InternalServerError(err));
      }
    };
  }

  /**
   * Create new User
   * @type {Function}
   */
  create() {
    return async (req, res, next) => {
      try {
        const email = req.body.email;
        const existingUser = await this.options.service.findByEmail(email);

        if (!existingUser || existingUser.length === 0) {
          const User = await this.options.service.create(req.body);
          return res.status(httpStatus.CREATED).json(User);
        }

        return res.status(httpStatus.BAD_REQUEST).json({
          status: httpStatus.BAD_REQUEST,
          message: `User data with email address ${email} is already exists.`,
        });
      } catch (err) {
        return next(new InternalServerError(err));
      }
    };
  }

  update() {
    return async (req, res, next) => {
      try {
        const data = req.body;
        data._id = req.params.id; // eslint-disable-line

        const user = await this.options.service.update(data);

        return res.status(httpStatus.OK).json(user);
      } catch (err) {
        return next(new InternalServerError(err));
      }
    };
  }

  remove() {
    return async (req, res, next) => {
      try {
        const id = req.params.id;
        await this.options.service.remove(id);

        return res.status(httpStatus.OK).json({
          message: 'Success deleting user data',
        });
      } catch (err) {
        return next(new NotFoundError(err));
      }
    };
  }
}

export default new UserController({
  service,
});
