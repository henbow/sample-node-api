import mongoose from 'mongoose';
import uuid from 'uuid';
import User from '../user/model';
import { ModelNotFoundError, SessionNotValidError } from '../../lib/errors';

const ObjectId = mongoose.Schema.Types.ObjectId;

/**
 * Schema
 */
const SessionSchema = new mongoose.Schema({
  token: {
    type: String,
    unique: true,
    index: true,
    required: true,
  },
  valid: {
    type: Boolean,
    index: true,
    required: true,
  },
  user: {
    type: ObjectId,
    ref: 'User',
    index: true,
    required: true,
  },
  createdAt: Date,
  lastLogin: Date,
  callsCount: Number,
}, {
  collection: 'sessions',
});

/**
 * Instance methods
 */
SessionSchema.method({

});

/**
 * Static methods
 */
SessionSchema.statics = {

  /**
   * Get User entity by request token
   * @param token
   * @returns {*|Promise|Array|{index: number, input: string}}
   */
  async authenticate(token) {
    const session = await this.findOne({ token, valid: true }).exec();

    if (!session) {
      throw new ModelNotFoundError(`Session with token: ${token} is not found`);
    }

    if (!session.valid) {
      throw new SessionNotValidError(`Session with token: ${token} is no longer valid`);
    }

    session.lastLogin = new Date();
    session.callsCount += 1;
    session.save();
    return await User.findOne(session.user).exec();
  },

  /**
   * Generate new session token
   * @param user
   * @returns {Promise}
   */
  async generate(user) {
    const Session = this.model('Session');
    const session = new Session({
      token: uuid.v1(),
      valid: true,
      user,
      createdAt: new Date(),
      lastAccess: undefined,
      callsCount: 0,
    });

    return await session.save();
  },

  /**
   * Get session for specific user
   * @param  {User} user
   * @return {Promise}
   */
  async getFor(user) {
    let session;
    try {
      session = await this.findOne({ user: user.id, valid: true }).exec();
    } catch (err) {
      return null;
    }

    return session;
  },

};

export default mongoose.model('Session', SessionSchema);
