import express from 'express';

import controller from './controller';
import validation from './validation';
import { check } from '../../middleware/UtilsMiddleware';
import { protect } from '../../middleware/AuthMiddleware';

const api = express.Router();
const authRouter = express.Router();

authRouter.route('/login')
  .post(check(validation.login), controller.login());

authRouter.route('/logout')
  .get(protect, controller.logout());

api.use('/auth', authRouter);

export default api;
