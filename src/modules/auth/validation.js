const login = {
  email: {
    presence: true,
  },
  password: {
    presence: true,
  },
};

export default {
  login,
};
