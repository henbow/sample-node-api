import status from 'http-status';
import Session from '../session/model';
import User from '../user/model';
import { NotFoundError, AuthenticationError } from '../../lib/errors';
import constants from '../../config/constants';
import { get as config } from '../../config';

export class AuthController {
  constructor(options) {
    this.options = options;
  }

  login() {
    return async (req, res, next) => {
      try {
        const user = await this.options.User.findOne({
          email: req.body.email,
        });

        if (!user) {
          return next(new NotFoundError(`There's no such account with email ${req.body.email}`));
        }

        const matched = await user.checkPassword(req.body.password);

        if (!matched) {
          return next(new AuthenticationError('Invalid credentials'));
        }

        // @todo : Apply these rules
        // 1. When X-WishCloud-Token header is present
        //    check in the session whether its still valid or not
        //    when it's valid, immediately return response
        // 2. When user already has valid session, invalidate old one
        //    create and return new one
        // 3. Other wise simply return new one

        const session = await this.options.Session.generate(user);

        return res.status(200).json({
          id: user.id,
          username: user.username,
          email: user.email,
          role: user.role,
          token: session.token,
        });
      } catch (err) {
        return next(err);
      }
    };
  }

  // eslint-disable-next-line
  logout = () => async (req, res, next) => {
    try {
      const token = req.header(config(constants.CONFIG_TOKEN_HEADER));
      const session = await Session.findOne({ token });
      // when logout invalidate all tokens
      await Session.remove({ human: session.human });

      res.status(status.OK).json({
        message: 'Session successfully been removed',
      });
    } catch (err) {
      next(err);
    }
  };
}

export default new AuthController({
  User,
  Session,
});
