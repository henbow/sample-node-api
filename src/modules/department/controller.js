import httpStatus from 'http-status';

import service from './service';
import { InternalServerError, NotFoundError, ModelNotFoundError } from '../../lib/errors';

export class DepartmentController {
  constructor(options) {
    this.options = options;
  }

  /**
   * Get Department list.
   * @return {Function}
   */
  list() {
    return async (req, res, next) => {
      try {
        const { limit = 10, skip = 0, ...restQuery } = req.query;

        const departments = await this.options.service.list({
          limit: parseInt(limit, 10),
          skip: parseInt(skip, 10),
          restQuery,
        });

        return res.status(httpStatus.OK).json({
          query: restQuery,
          limit: parseInt(limit, 10),
          skip: parseInt(skip, 10),
          totalResults: departments.length,
          results: departments,
        });
      } catch (err) {
        return next(new InternalServerError(err));
      }
    };
  }

  show() {
    return async (req, res, next) => {
      try {
        const id = req.params.id;
        const department = await this.options.service.findById(id);

        return res.status(httpStatus.OK).json(department);
      } catch (err) {
        if (err instanceof ModelNotFoundError) {
          return next(new NotFoundError(`Department with id ${req.params.id} is not found`, err));
        }

        return next(new InternalServerError(err));
      }
    };
  }

  /**
   * Create new Department
   * @type {Function}
   */
  create() {
    return async (req, res, next) => {
      try {
        const deptName = req.body.name;
        const existingDepartment = await this.options.service.findByName(deptName);

        if (!existingDepartment || existingDepartment.length === 0) {
          const department = await this.options.service.create(req.body);
          return res.status(httpStatus.CREATED).json(department);
        }

        return res.status(httpStatus.BAD_REQUEST).json({
          status: httpStatus.BAD_REQUEST,
          message: `${deptName} department is already exists.`,
        });
      } catch (err) {
        return next(new InternalServerError(err));
      }
    };
  }

  update() {
    return async (req, res, next) => {
      try {
        const data = req.body;

        // eslint-disable-next-line no-underscore-dangle
        let id = data._id;

        if (!id) {
          id = req.params.id;
        }

        const department = await this.options.service.update(id, data);

        return res.status(httpStatus.OK).json(department);
      } catch (err) {
        return next(new InternalServerError(err));
      }
    };
  }
}

export default new DepartmentController({
  service,
});
