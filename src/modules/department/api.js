import express from 'express';

import controller from './controller';
import { protect } from '../../middleware/AuthMiddleware';

const api = express.Router();
const departmentRouter = express.Router();

departmentRouter.route('/')
  .get(protect, controller.list())
  .post(protect, controller.create());

departmentRouter.route('/:id')
  .get(protect, controller.show())
  .put(protect, controller.update());
  // .delete();

api.use('/department', departmentRouter);

export default api;
