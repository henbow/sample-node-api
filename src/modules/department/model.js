import mongoose from 'mongoose';

/**
 * Department Schema
 */
export const DepartmentSchema = new mongoose.Schema({
  _type: { type: String, default: 'Department' },
  name: String,
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
}, {
  collection: 'departments',
});

/**
 * Instance methods
 */
DepartmentSchema.method({

});

/**
 * Static methods
 */
DepartmentSchema.statics = {

};

export default mongoose.model('Department', DepartmentSchema);
