import moment from 'moment';

import Department from './model';
import { NoResultFoundError, ModelNotFoundError } from '../../lib/errors';

export class DepartmentService {
  constructor(options) {
    this.options = options;
  }

  async list({ limit = 50, skip = 0, query = {} } = {}) {
    try {
      const results = await this.options.Department.find(query)
        .sort({ createdAt: -1 })
        .limit(limit)
        .skip(skip)
        .exec();

      return results;
    } catch (err) {
      throw new NoResultFoundError('No results found!', 'Department');
    }
  }

  async findById(id) {
    try {
      return await this.options.Department.findById(id);
    } catch (err) {
      throw new ModelNotFoundError(err.message);
    }
  }

  async findByName(deptName) {
    try {
      return await this.options.Department.findOne({ name: deptName });
    } catch (err) {
      throw new ModelNotFoundError(err.message);
    }
  }

  async create(data) {
    try {
      const department = this.options.Department;
      return await department.create(data);
    } catch (err) {
      throw err;
    }
  }

  async update(data) {
    try {
      // eslint-disable-next-line
      const id = data._id;

      data.updatedAt = moment().toISOString();

      return await this.options.Department.findOneAndUpdate(
        { _id: id },
        { $set: data },
        { new: true },
      );
    } catch (err) {
      throw err;
    }
  }
}

export default new DepartmentService({
  Department,
});
