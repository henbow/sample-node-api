import c from '../../config/constants';
import { config, getConfigFiles } from '../../config/index';
import { requestLoggerMiddleware, timeoutMiddleware } from './middleware';
import container from './container';
import api from './api';

function configure(app) {
  app.on(c.MODULE_ON_INIT, () => {
    // load default config files with strict validation
    config.loadFile(getConfigFiles());
    config.validate({ strict: true });

    // enable request logger and timeout middleware
    app.use(requestLoggerMiddleware());
    app.use(timeoutMiddleware(1000));
  });

  app.on(c.MODULE_ON_EXPOSE, () => {
    app.use(api);
  });
}

export default { configure, container };
