/* eslint-disable */
import definitions from '../../../src/config/definitions';

describe('test for definitions', () => {

  test('definitions should be an object', () => {
    expect(definitions).toBeInstanceOf(Object);
  })

})
