/* eslint-disable */
jest.mock('mongoose');
import 'babel-polyfill';
import * as connect from '../../../../../src/lib/persistence/mongo/connect';

describe('test mongo connection functions', () => {

  test('connectAsync function should works as expected', async () => {
    const conn = await connect.connectAsync('mongo://user:pass@localhost:9000');
    expect(conn).toBeUndefined(); // resolves nothing
  })

})
