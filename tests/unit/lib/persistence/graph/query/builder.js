/* eslint-disable */
import * as builder from '../../../../../../src/lib/persistence/graph/query/builder';

describe('test cypher query builder', () => {

  test('ErrorFactory should produce proper error message', () => {
    const invalidParameterType = builder.ErrorFactory.createInvalidParameterTypeError;
    expect(invalidParameterType('node', 'Node').message).toEqual('node must be type of Node');
  })

  test('Raw class should produce correct query', () => {
    const raw = new builder.Raw('raw query');
    expect(raw.query).toEqual('raw query');

    raw.config.expr = 'another raw query';
    expect(raw.config.expr).toEqual('another raw query');
    expect(raw.query).toEqual('another raw query');

    raw.expr('my raw query');
    expect(raw.config.expr).toEqual('my raw query');
    expect(raw.query).toEqual('my raw query');
  })

  test('Node class should produce correct query', () => {
    const node = new builder.Node('p', 'Person:Manager', {name: 'John', age: 25, sex: 'male'});
    const query1 = `(p:Person:Manager {name: 'John', age: 25, sex: 'male'})`;

    expect(node.query).toEqual(query1);
    expect(node.config.labels.length).toEqual(2);

    const date = new Date(2016, 1, 2);
    const rel = new builder.Relation('r', 'KNOWS', {since: date.toString()}, 'right');

    node.rel(rel);

    expect(node.query).toEqual(`${query1}-[r:KNOWS {since: '${date.toString()}'}]->`);

    const knowsNode = new builder.Node('p', ['Person', 'Employee'], {name: 'Alan', age: 29});
    const query2 = `(p:Person:Employee {name: 'Alan', age: 29})`;

    rel.node(knowsNode);
    expect(node.query).toEqual(`${query1}-[r:KNOWS {since: '${date.toString()}'}]->${query2}`);

    node.reset();
    expect(node.query).toEqual(query1);

    node.rel(knowsNode);
    expect(node.query).toEqual(`${query1}--${query2}`);

    node.reset();
    node.rel(['-->', knowsNode]);
    expect(node.query).toEqual(`${query1}-->${query2}`);

    node.reset();
    node.rel(['<--', knowsNode]);
    expect(node.query).toEqual(`${query1}<--${query2}`);

    node.reset();
    expect(() => { node.rel(['<-->', knowsNode]) }).toThrow();

    const node2 = new builder.Node(null, ['Human', 'Person']);
    expect(node2.query).toEqual(`(:Human:Person)`);
  })

  test('Match class should produce correct query', () => {
    const match = new builder.Match();
    const node1 = new builder.Node('p', 'Person', {name: 'John'});
    const node2 = new builder.Node('p', 'Person', {name: 'Alice'});
    const node3 = new builder.Node('p', 'Person', {name: 'Marc'});

    match.nodes([node1, node2]);
    expect(match.config.nodes.length).toEqual(2);
    expect(() => { match.nodes('invalid nodes') }).toThrow();
    expect(() => { match.add({}) }).toThrow();
    match.add(node3);
    expect(match.config.nodes.length).toEqual(3);

    match.filter(n => n.config.properties.name === 'John');
    expect(match.config.nodes.length).toEqual(1);
    expect(match.query).toEqual(`MATCH (p:Person {name: 'John'})`);

    const match2 = new builder.Match(true);
    match2.add(node3);

    expect(match2.query).toEqual(`OPTIONAL MATCH (p:Person {name: 'Marc'})`);

    match2.filter(n => false);
    expect(match2.query).toEqual('');

    match2.add(new builder.Raw('(n)'));
    expect(match2.query).toEqual(`OPTIONAL MATCH (n)`);
  })

  test('Relation class should produce correct query', () => {
    const rel = new builder.Relation('r', 'KNOWS', {since: '2016-02-20'}, 'right', '1**3');
    expect(rel.config.direction).toEqual('right');

    rel.dir('left');
    expect(rel.config.direction).toEqual('left');
    expect(() => { rel.dir('invalid') }).toThrow();

    rel.range('1**5');
    expect(rel.config.range).toEqual('1**5');

    const node1 = new builder.Node('p', 'Person', {name: 'John'});
    const node2 = new builder.Node('p', 'Person', {name: 'Alice'});

    rel.node(node1);
    expect(rel.query).toEqual(`<-[r:KNOWS {since: '2016-02-20'}1**5]-(p:Person {name: 'John'})`);

    node1.rel(node2);
    expect(rel.query).toEqual(`<-[r:KNOWS {since: '2016-02-20'}1**5]-(p:Person {name: 'John'})--(p:Person {name: 'Alice'})`);

    rel.node(null);
    rel.dir('right');
    expect(rel.query).toEqual(`-[r:KNOWS {since: '2016-02-20'}1**5]->`);

    rel.dir('omni');
    expect(rel.query).toEqual(`-[r:KNOWS {since: '2016-02-20'}1**5]-`);
  })

  test('Where class should produce correct query', () => {
    const expr = `p.name = 'John'`;
    const where = new builder.Where(expr);
    expect(where.config.expr).toEqual(expr);
    expect(where.query).toEqual(`WHERE ${expr}`);

    where.expr('some expression');
    expect(where.query).toEqual(`WHERE some expression`);
  })

  test('OrderBy class should produce correct query', () => {
    const orderBy = new builder.OrderBy('p.name', 'asc');
    expect(orderBy.config.fields).toEqual([['p.name', 'asc']]);
    expect(orderBy.config.fields.length).toEqual(1);
    expect(orderBy.fields().length).toEqual(1);

    orderBy.add('p.age', 'desc');
    expect(orderBy.fields().length).toEqual(2);
    expect(orderBy.query).toEqual(`ORDER BY p.name asc, p.age desc`);
  })

  test('Create class should produce correct query', () => {
    const node1 = new builder.Node('p', 'Person', {name: 'John'});
    const node2 = new builder.Node('p', 'Person', {name: 'Alice'});

    const create1 = new builder.Create(node1);
    const create2 = new builder.Create([node1, node2]);

    expect(create1.config.nodes.length).toEqual(1);
    expect(create2.config.nodes.length).toEqual(2);

    create1.add(node2);
    expect(create1.config.nodes.length).toEqual(2);
    expect(() => { create1.add('expect an error') }).toThrow();

    create1.returns('p');
    expect(create1.query).toEqual(`CREATE (p:Person {name: 'John'}), (p:Person {name: 'Alice'}) RETURN p`);

    create1.returns(['p']);
    expect(create1.query).toEqual(`CREATE (p:Person {name: 'John'}), (p:Person {name: 'Alice'}) RETURN p`);
  })

  test('MatchBuilder class should produce correct query', () => {
    const match1 = new builder.Match();
    const match2 = new builder.Match();
    const node1 = new builder.Node('p', 'Person');
    const node2 = new builder.Node('u', 'User');
    const node3 = new builder.Node('g', 'Guest');

    match1.add(node1);
    match2.nodes([node2, node3]);

    const b = new builder.MatchBuilder(match1);

    b.add(match2)
      .where(new builder.Where(`p.name = 'John'`));

    expect(b.config.where.config.expr).toEqual(`p.name = 'John'`);

    b.where(`p.name = 'Alice'`);
    expect(b.config.where.config.expr).toEqual(`p.name = 'Alice'`);

    b.where((where) => {
      where.expr(`p.name = 'Marc'`);
    });
    expect(b.config.where.config.expr).toEqual(`p.name = 'Marc'`);

    b.orderBy(new builder.OrderBy('p.name', 'asc'));
    expect(b.config.orderBy.fields().length).toEqual(1);

    b.orderBy('p.name', 'desc');
    expect(b.config.orderBy.config.fields[0]).toEqual(['p.name', 'asc']);
    expect(b.config.orderBy.config.fields[1]).toEqual(['p.name', 'desc']);

    b.orderBy((orderBy) => {
      orderBy.add('p.age', 'desc');
    });
    expect(b.config.orderBy.fields().length).toEqual(3);

    b.order((orderBy) => {
      orderBy.add('p.joined', 'desc');
    });
    expect(b.config.orderBy.fields().length).toEqual(4);

    expect(() => { b.create() }).toThrow();

    b.skip(0);
    expect(b.config.skip).toEqual(0);

    b.limit(10);
    expect(b.config.limit).toEqual(10);

    b.returns('a');
    expect(b.config.returns.length).toEqual(1);

    b.returns(['p', 'u']);
    expect(b.config.returns.length).toEqual(2);

    expect(b.query).toEqual(`MATCH (p:Person) MATCH (u:User), (g:Guest) WHERE p.name = 'Marc' RETURN p, u ORDER BY p.name asc, p.name desc, p.age desc, p.joined desc LIMIT 10`);
  })

})
