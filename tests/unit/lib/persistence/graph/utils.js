/* eslint-disable */
import * as utils from '../../../../../src/lib/persistence/graph/utils';
import * as builder from '../../../../../src/lib/persistence/graph/query/builder';

describe('test graph util functions', () => {

  test('Cypher class should works as expected', () => {
    const runMockFn = jest.fn().mockImplementation(() => {
      return {
        subscribe: () => {}
      }
    });

    const sessionMockFn = jest.fn().mockImplementation(() => {
      return {
        run: runMockFn
      }
    });

    const neo4j = {
      session: sessionMockFn
    };

    const cypher = new utils.Cypher(neo4j);
    cypher.query('statement', { foo: 'bar' });

    expect(sessionMockFn.mock.calls.length).toEqual(1);
    expect(runMockFn.mock.calls.length).toEqual(1);
    expect(runMockFn).toHaveBeenCalledWith('statement', { foo: 'bar' });
    expect(sessionMockFn).toHaveBeenCalled();
  })

  test('objectToStringProps function should works as expected', () => {
    const str1 = utils.objectToStringProps({foo: 'bar'});
    expect(str1).toEqual(`foo: 'bar'`);

    const str2 = utils.objectToStringProps(`{"foo": "bar"}`);
    expect(str2).toEqual(`foo: 'bar'`);
  })

  test('labelToArray function should works as expected', () => {
    const labels1 = utils.labelToArray(['label1', 'label2']);
    expect(labels1.length).toEqual(2);
    expect(labels1[0]).toEqual('label1');

    const labels2 = utils.labelToArray('label1:label2');
    expect(labels2.length).toEqual(2);
    expect(labels2[0]).toEqual('label1');
  })

  test('uuid function should works as expected', () => {
    const uuid = utils.uuid();
    expect(uuid.length).toEqual(36);
  })

  test('n function should works as expected', () => {
    const n = utils.n('p', 'Person', { name : 'John' });
    expect(n.query).toEqual(`(p:Person {name: 'John'})`);
  })

  test('r function should works as expected', () => {
    const r = utils.r('r', 'KNOWS', { since : '2016-10-21' }, 'omni', '1**3');
    expect(r.query).toEqual(`-[r:KNOWS {since: '2016-10-21'}1**3]-`);
  })

  test('match function should works as expected', () => {
    const m1 = utils.match(new builder.Match());
    expect(m1).toBeInstanceOf(builder.MatchBuilder);

    const m2 = utils.match(new builder.Node('p', 'Person'));
    expect(m2).toBeInstanceOf(builder.MatchBuilder);
  })

})
