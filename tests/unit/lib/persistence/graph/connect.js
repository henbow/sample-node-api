/* eslint-disable */
jest.mock('neo4j-driver');
import 'babel-polyfill';
import * as connect from '../../../../../src/lib/persistence/graph/connect';

describe('test graph connection functions', () => {

  test('connectAsync function should works as expected', async () => {
    const conn = await connect.connectAsync('bolt://user:pass@localhost:9000');
    expect(conn).toEqual('bolt://localhost:9000');
  })

  test('connect function should works as expected', () => {
    const conn = connect.connect('bolt://user:pass@localhost');
    expect(conn).toEqual('bolt://localhost:7687');
  })

})
