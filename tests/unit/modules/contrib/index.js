/* eslint-disable */
import contrib from '../../../../src/modules/contrib';
import { isFunction, isObject } from '../../../../src/lib/utils';

describe('contrib module test', () => {

  test('validation must have template object', () => {
    expect(isObject(contrib.validation)).toBeTruthy();
    expect(isObject(contrib.validation.Template)).toBeTruthy();
  })

  test('configure must be instance of function', () => {
    expect(isFunction(contrib.configure)).toBeTruthy();
  })

})
