/* eslint-disable */
import core from '../../../../src/modules/core';
import { isFunction, isObject } from '../../../../src/lib/utils';

describe('core module test', () => {

  test('container must be instance of Container class', () => {
    expect(isObject(core.container)).toBeTruthy();
    expect(typeof core.container.get === 'function').toBeTruthy();
  })

  test('configure must be instance of function', () => {
    expect(isFunction(core.configure)).toBeTruthy();
  })

})
